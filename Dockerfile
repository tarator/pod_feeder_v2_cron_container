FROM alpine:latest

RUN apk add \
    bash \
    git \
    python3 \
    python3-dev


RUN cd /home && \
    git clone https://gitlab.com/tarator/pod_feeder_v2.git pod_feeder_v2 && \
    cd pod_feeder_v2 && \
    pip3 install --upgrade pip && \
    pip3 install -r /home/pod_feeder_v2/requirements.txt && \
    cd /usr/bin/ && \
    ln -s /home/pod_feeder_v2/pod_feeder.py pod_feeder.py  && \
    ln -s /home/pod_feeder_v2/clean_db.py clean_db.py 

RUN mkdir /data && \
    cp /etc/crontabs/root /data/crontab

ADD entrypoint.sh /usr/bin/
RUN chmod 555 /usr/bin/entrypoint.sh

VOLUME [ "/data" ]

ENTRYPOINT [ "/usr/bin/entrypoint.sh" ]
CMD ["/usr/sbin/crond","-f"]
