# pod_feeder_v2 cron container

Docker Image based on Alpine Linux running the [pod_feeder_v2](https://gitlab.com/brianodonnell/pod_feeder_v2), a tool to publish RSS/Atom Feeds to a [Diaspora](https://diasporafoundation.org/) Pod.


## Build the docker image:

Run `docker build -t pod-feeder-v2-cron-container .` to build the Docker image.

## Run the container with Docker-compose

Take a look into the folder `docker-compose-example` for an example setup.
Make sure, that you set the database path in the `crontab` file to the right directory (`data/feeder.db`)
Use the file `podFeederData/crontab` to setup your feeds. You need to restart the container when you change the contents of the `crontab`-file


Run the container with `docker-compose up -d`. The database `feeder.db` will be in the `podFeederData` on your host-machine.

Here is a `docker-compose.yml` which pulls the Container from Gitlab's container registry:


    version: '2'
    services:
      pod-feeder:
        image: registry.gitlab.com/tarator/pod_feeder_v2_cron_container:latest
        volumes:
          - "./podFeederData:/data"
        restart: always


Test 123
