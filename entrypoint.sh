#!/usr/bin/env bash
set -e

echo "Copying crontab file from /data/ directory..."
rm /etc/crontabs/root
cp /data/crontab /etc/crontabs/root

echo "Startinfg crond..."
exec "$@"
